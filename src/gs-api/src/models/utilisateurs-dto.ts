/* tslint:disable */
import { RolesDto } from './roles-dto';
export interface UtilisateursDto {
  activer?: boolean;
  email?: string;
  id?: number;
  login?: string;
  nom?: string;
  password?: string;
  roles?: Array<RolesDto>;
  telephone?: string;
}
