/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { UtilisateursDto } from '../models/utilisateurs-dto';
import { MessageNotification } from '../models/message-notification';
import { UserSaveBean } from '../models/user-save-bean';
@Injectable({
  providedIn: 'root',
})
class UtilisateurApiService extends __BaseService {
  static readonly findAllPath = '/api/skysoft/v1/users/findAll';
  static readonly savePath = '/api/skysoft/v1/users/save';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Lister les utilisateurs du système
   *
   * Cette méthode permet de lister les utilisateurs du système
   * @param params The `UtilisateurApiService.FindAllParams` containing the following parameters:
   *
   * - `typeClassement`:
   *
   * - `type`:
   *
   * - `telephone`:
   *
   * - `nombreDeResultat`:
   *
   * - `nom`:
   *
   * - `login`:
   *
   * - `email`:
   *
   * - `classement`:
   *
   * @return Liste vide ou une liste d'utilisateur retournée
   */
  findAllResponse(params: UtilisateurApiService.FindAllParams): __Observable<__StrictHttpResponse<Array<UtilisateursDto>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.typeClassement != null) __params = __params.set('typeClassement', params.typeClassement.toString());
    if (params.type != null) __params = __params.set('type', params.type.toString());
    if (params.telephone != null) __params = __params.set('telephone', params.telephone.toString());
    if (params.nombreDeResultat != null) __params = __params.set('nombreDeResultat', params.nombreDeResultat.toString());
    if (params.nom != null) __params = __params.set('nom', params.nom.toString());
    if (params.login != null) __params = __params.set('login', params.login.toString());
    if (params.email != null) __params = __params.set('email', params.email.toString());
    if (params.classement != null) __params = __params.set('classement', params.classement.toString());
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/skysoft/v1/users/findAll`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<Array<UtilisateursDto>>;
      })
    );
  }
  /**
   * Lister les utilisateurs du système
   *
   * Cette méthode permet de lister les utilisateurs du système
   * @param params The `UtilisateurApiService.FindAllParams` containing the following parameters:
   *
   * - `typeClassement`:
   *
   * - `type`:
   *
   * - `telephone`:
   *
   * - `nombreDeResultat`:
   *
   * - `nom`:
   *
   * - `login`:
   *
   * - `email`:
   *
   * - `classement`:
   *
   * @return Liste vide ou une liste d'utilisateur retournée
   */
  findAll(params: UtilisateurApiService.FindAllParams): __Observable<Array<UtilisateursDto>> {
    return this.findAllResponse(params).pipe(
      __map(_r => _r.body as Array<UtilisateursDto>)
    );
  }

  /**
   * Créer un nouvel utilisateur
   *
   * Cette méthode permet de créer un nouvel utilisateur dans le système
   * @param params The `UtilisateurApiService.SaveParams` containing the following parameters:
   *
   * - `language`:
   *
   * - `body`:
   *
   * @return L'utilisateur a été créé avec succès
   */
  saveResponse(params: UtilisateurApiService.SaveParams): __Observable<__StrictHttpResponse<MessageNotification>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    if (params.language != null) __params = __params.set('language', params.language.toString());
    __body = params.body;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/skysoft/v1/users/save`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<MessageNotification>;
      })
    );
  }
  /**
   * Créer un nouvel utilisateur
   *
   * Cette méthode permet de créer un nouvel utilisateur dans le système
   * @param params The `UtilisateurApiService.SaveParams` containing the following parameters:
   *
   * - `language`:
   *
   * - `body`:
   *
   * @return L'utilisateur a été créé avec succès
   */
  save(params: UtilisateurApiService.SaveParams): __Observable<MessageNotification> {
    return this.saveResponse(params).pipe(
      __map(_r => _r.body as MessageNotification)
    );
  }
}

module UtilisateurApiService {

  /**
   * Parameters for findAll
   */
  export interface FindAllParams {
    typeClassement?: string;
    type?: string;
    telephone?: string;
    nombreDeResultat?: number;
    nom?: string;
    login?: string;
    email?: string;
    classement?: boolean;
  }

  /**
   * Parameters for save
   */
  export interface SaveParams {
    language?: string;
    body?: UserSaveBean;
  }
}

export { UtilisateurApiService }
