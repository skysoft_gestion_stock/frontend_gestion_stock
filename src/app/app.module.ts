import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SharedModule} from "./shared/shared.module";
import {CoreModule} from "./core/core.module";
import {HttpClient} from "@angular/common/http";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {LoadingBarHttpClientModule} from "@ngx-loading-bar/http-client";
import {DateFormatPipe} from "./shared/pipe/DateFormatPipe";
import {DateFormatUpdatePipe} from "./shared/pipe/DateFormatUpdatePipe";
import {DateHourFormatPipe} from "./shared/pipe/DateHourFormatPipe";
import {ToastrModule} from "ngx-toastr";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    LoadingBarHttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ToastrModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    DateFormatPipe,
    DateFormatUpdatePipe,
    DateHourFormatPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
