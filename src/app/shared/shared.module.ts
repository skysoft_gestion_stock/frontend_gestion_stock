import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MyDatePipe} from "./pipe/myDatePipe.pipe";
import {NgSelectModule} from "@ng-select/ng-select";
import {FormValidationComponent} from "../component-global/form-validation/form-validation.component";
import {TranslateModule} from "@ngx-translate/core";
import {DateFormatPipe} from "./pipe/DateFormatPipe";
import {DateFormatUpdatePipe} from "./pipe/DateFormatUpdatePipe";
import {DateHourFormatPipe} from "./pipe/DateHourFormatPipe";

@NgModule({
  declarations: [
    MyDatePipe,
    DateFormatPipe,
    DateFormatUpdatePipe,
    DateHourFormatPipe,
    FormValidationComponent
  ],
  imports: [
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    TranslateModule,
    CommonModule
  ],
  exports: [
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    TranslateModule,
    CommonModule,
    MyDatePipe,
    DateFormatPipe,
    DateFormatUpdatePipe,
    DateHourFormatPipe,
    FormValidationComponent
  ]
})
export class SharedModule { }
