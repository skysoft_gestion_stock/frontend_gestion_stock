import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {WebStorageService} from "./web-storage.service";
import {AccountService} from "./account.service";
import {map} from "rxjs/operators";
import {AuthenticateApiService} from "../../../gs-api/src/services/authenticate-api.service";
import {AuthenticationRequestBean} from "../../../gs-api/src/models/authentication-request-bean";
import {AuthenticationResponseBean} from "../../../gs-api/src/models/authentication-response-bean";
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class AuthJwtService {

  constructor(
    private webStorageService: WebStorageService,
    private router: Router,
    private toastService: ToastrService,
    private accountService: AccountService,
    private authenticationService: AuthenticateApiService
  ) {
  }




  isAuthentificated(): boolean {
    if (this.webStorageService.getToken()) {
      return true;
    }
    this.router.navigate(['login']).then(() => {
    });
    return false;
  }

  logout(): void {
    this.authenticationService.logout().subscribe({
      next: () => {
        this.router.navigate(['login']).then(() => {
        });
        this.webStorageService.clearToken();
      }
    });
  }

  logoutWithoutObserver(): void {
    this.webStorageService.clearToken();
    this.accountService.authenticate(null);
    this.router.navigate(['login']).then(() => {
    });
  }

  public authenficationSuccess(authentificationResponse: AuthenticationResponseBean): void {
    this.accountService.identity(true);
    this.webStorageService.storeToken(authentificationResponse.accessToken, true);
    this.toastService.success(authentificationResponse.message);
  }


}
