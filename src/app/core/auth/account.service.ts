import {Injectable} from '@angular/core';
import {BehaviorSubject, catchError, Observable, of, ReplaySubject, shareReplay, tap} from "rxjs";
import {UtilisateursDto} from "../../../gs-api/src/models/utilisateurs-dto";
import {MessageNotification} from "../../../gs-api/src/models/message-notification";
import {RolesDto} from "../../../gs-api/src/models/roles-dto";
import {AuthenticateApiService} from "../../../gs-api/src/services/authenticate-api.service";
import ChangePasswordParams = AuthenticateApiService.ChangePasswordParams;

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private userIdentity: UtilisateursDto | null = null;
  private authenticationState = new ReplaySubject<UtilisateursDto | null>(1);
  private accountCache$?: Observable<UtilisateursDto | null>;
  private currentUserSubject = new BehaviorSubject<UtilisateursDto | null>({});

  constructor(
    public authenticateApiService: AuthenticateApiService
  ) {
  }

  getCurrentUser(): Observable<UtilisateursDto> {
    return this.authenticateApiService.currentUser();
  }

  hasAnyAuthority(authorities: string[] | string | undefined): boolean {
    if (!this.userIdentity || !this.userIdentity.roles) {
      return false;
    }
    if (!Array.isArray(authorities)) {
      authorities = [authorities ?? ''];
    }
    return this.userIdentity.roles.some((authority: RolesDto) => authorities?.includes(authority.nom ?? ''));
  }

  identity(force?: boolean): Observable<UtilisateursDto | null> {
    if (!this.accountCache$ || force || !this.isAuthenticated()) {
      this.accountCache$ = this.getCurrentUser().pipe(
        catchError(() => of(null)),
        tap((account: UtilisateursDto | null) => {
          this.authenticate(account);
        }),
        shareReplay()
      );
    }
    return this.accountCache$;
  }

  authenticate(identity: UtilisateursDto | null): void {
    this.userIdentity = identity;
    this.authenticationState.next(this.userIdentity);
    this.currentUserSubject.next(this.userIdentity);
  }

  isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }

  getAuthenticationState(): Observable<UtilisateursDto | null> {
    return this.authenticationState.asObservable();
  }

  getCurrentUserValue(): UtilisateursDto | null {
    return this.currentUserSubject.value;
  }

  changePassword(changePassword: ChangePasswordParams): Observable<MessageNotification> {
    return this.authenticateApiService.changePassword(changePassword);
  }

}
