import { Injectable } from '@angular/core';
import {LocalStorageService, SessionStorageService} from "ngx-webstorage";
import {AgenceDto} from "../../../gs-api/src/models/agence-dto";

@Injectable({
  providedIn: 'root'
})
export class WebStorageService {

  constructor(
      private $localStorage: LocalStorageService,
      private $sessionsStorage: SessionStorageService
  ) { }

  storeToken(token?: string, remember?: boolean): void {
    if (remember){
      this.$localStorage.store("access_token_care_cameroon", token);
    } else {
      this.$sessionsStorage.store("access_token_care_cameroon", token);
    }
  }

  getLanguage(): string {
    return localStorage.getItem('language') ?? 'fr';
  }

  getToken(): string {
    if (this.$sessionsStorage.retrieve("access_token_care_cameroon")) {
      return this.$sessionsStorage.retrieve("access_token_care_cameroon");
    }
    else {
      return this.$localStorage.retrieve("access_token_care_cameroon");
    }
  }

  clearToken(): void {
    this.$sessionsStorage.clear("access_token_care_cameroon");
    this.$localStorage.clear("access_token_care_cameroon");
  }

  setAgenceSelected(agence: AgenceDto){
    this.$sessionsStorage.store('agence_selected', JSON.stringify(agence))
  }

  public getAgenceSelected(): AgenceDto{
    return JSON.parse(this.$sessionsStorage.retrieve('agence_selected'))
  }

  setCurrentUser(current_user?: string): void {
    this.$localStorage.store("access_current_user", current_user);
  }

  getCurrentUser(): string {
    return this.$localStorage.retrieve("access_current_user");
  }

}
