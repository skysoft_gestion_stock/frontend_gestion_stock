import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {layoutsRouters} from "./layouts/layouts.route";

const routes: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  ...layoutsRouters,
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule {
}
