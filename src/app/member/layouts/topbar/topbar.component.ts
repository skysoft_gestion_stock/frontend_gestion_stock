import {Component, OnInit} from '@angular/core';
import {AccountService} from "../../../core/auth/account.service";
import {LanguageService} from "../../../core/service/language.service";
import {Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {UtilisateursDto} from "../../../../gs-api/src/models/utilisateurs-dto";
import {AuthJwtService} from "../../../core/auth/auth-jwt.service";
import {WebStorageService} from "../../../core/auth/web-storage.service";

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  logo_care_cameroon = "assets/img/logo.png";

  undraw_profile = "assets/img/undraw_profile.svg";

  contry_flag_us = "assets/img/flags/us.svg";

  contry_flag_fr = "assets/img/flags/french.svg";

  userDto: UtilisateursDto | null = {};
  countryName: string[] = [];
  flag: string[] = [];
  langStoreValue: string = '';

  listLang = [
    {text: 'French', flag: 'assets/img/flags/french.svg', lang: 'fr'},
    {text: 'English', flag: 'assets/img/flags/us.svg', lang: 'en'},
  ];

  gridListings: number | undefined;

  constructor(
    private accountService: AccountService,
    private languageService: LanguageService,
    private webStorageService: WebStorageService,
    private router: Router,
    private authJwtService: AuthJwtService,
    private ngbModal: NgbModal
  ) {
  }

  ngOnInit(): void {
    this.accountService.identity().subscribe((userDto: UtilisateursDto | null) => {
      this.userDto = userDto;
    });
    this.langStoreValue = localStorage.getItem("language") ?? 'fr';
    const val = this.listLang.filter((x) => x.lang === this.langStoreValue);
    this.countryName = val.map((element) => element.text);
    this.flag = val.map((element) => element.flag);
    this.languageService.setLanguage(this.langStoreValue);
  }


  // changePassword(loginUser: string | null | undefined): void {
  //   const modalRef = this.ngbModal.open(ChangePasswordComponent, {size: 'md', backdrop: 'static'});
  //   modalRef.componentInstance.loginUser = loginUser;
  // }

  setLanguage(text: string, flag: string, lang: string) {
    this.countryName[0] = text;
    this.flag[0] = flag;
    this.langStoreValue = lang;
    this.languageService.setLanguage(lang);
  }

  logout(): void {
    this.authJwtService.logout();
  }

  toggle() {
    const element = document.body as HTMLBodyElement;
    element.classList.toggle('toggle-sidebar');
  }


}
