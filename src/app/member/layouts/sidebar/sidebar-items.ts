import {SidebarModel} from "./sidebar.model";

export const ROUTES: SidebarModel[] = [
  {
    menu: 'sidebar.dashboard',
    path: 'dashboard',
    data_target: 'dashboard',
    icon: 'bi bi-grid',
    roles: [],
    title_submenu: 'sidebar.administration_title_sub_menu',
    submenu: [
    ],
  },
  {
    menu: 'sidebar.contact_management',
    path: '',
    data_target: 'gestion',
    icon: 'bi bi-briefcase',
    roles: [],
    title_submenu: 'sidebar.gestion_title_sub_menu',
    submenu: [
      {
        menu: 'Contacts',
        path: 'contacts',
        icon: 'bi bi-dash',
        data_target: 'gestion-contacts',
      },
      {
        menu: 'sidebar.sender',
        path: 'expediteurs',
        icon: 'bi bi-dash',
        data_target: 'gestion-expediteurs',
      },
      {
        menu: 'sidebar.import',
        path: 'contacts/import',
        icon: 'bi bi-dash',
        data_target: 'gestion-contacts',
      },
      {
        menu: 'sidebar.import-group',
        path: 'contacts/import-group',
        icon: 'bi bi-dash',
        data_target: 'gestion-contacts',
      },
      {
        menu: 'sidebar.group',
        path: 'group_contacts',
        icon: 'bi bi-dash',
        data_target: 'gestion-groupe',
      },
    ]
  },
  {
    menu: 'sidebar.send_sms',
    path: '',
    data_target: 'sms',
    icon: 'bi bi-briefcase',
    roles: [],
    title_submenu: 'sidebar.gestion_title_sub_menu',
    submenu: [
      {
        menu: 'sidebar.simple_sms',
        path: 'sms/simple',
        icon: 'bi bi-dash',
        data_target: 'gestion-contacts',
      },{
        menu: 'sidebar.sms_perso',
        path: 'sms/personaliser',
        icon: 'bi bi-dash',
        data_target: 'gestion-contacts',
      },
    ]
  },
  {
    menu: 'sidebar.administration',
    path: '',
    data_target: 'administration',
    icon: 'bi bi-person',
    roles: [],
    title_submenu: 'sidebar.administration_title_sub_menu',
    submenu: [
      {
        menu: 'sidebar.developer',
        path: 'developer',
        icon: 'bi bi-circle',
        roles: [],
        submenu: []
      },
    ]
  }
];
