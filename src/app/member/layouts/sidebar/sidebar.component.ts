import {Component, OnInit} from '@angular/core';
import {ROUTES} from './sidebar-items';
import {AccountService} from "../../../core/auth/account.service";
import {UtilisateursDto} from "../../../../gs-api/src/models/utilisateurs-dto";
import {LogoutModalComponent} from "../logout-modal/logout-modal.component";
import {LanguageService} from "../../../core/service/language.service";
import {Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  title = 'care_cameroun_frontend';

  undraw_profile = "assets/img/undraw_profile.svg";


  userDto: UtilisateursDto | null = {};

  menus = ROUTES;

  constructor(
    private accountService: AccountService,
    private languageService: LanguageService,
    private router: Router,
    private ngbModal: NgbModal,
  ) {
  }

  ngOnInit(): void {
    this.accountService.identity().subscribe((userDto: UtilisateursDto | null) => {
      this.userDto = userDto;
    });
  }

  changePassword(loginUser: string | null | undefined): void {
    // const modalRef = this.ngbModal.open(ChangePasswordComponent, {size: 'md', backdrop: 'static'});
    // modalRef.componentInstance.loginUser = loginUser;
  }

  logout(): void {
    this.ngbModal.open(LogoutModalComponent, {size: 'md', backdrop: false});
  }

  shouldActivateLink(path: string | undefined): string[] {
    return path ? [path] : [];
  }

}
