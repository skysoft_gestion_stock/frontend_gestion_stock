import { Component } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {AuthJwtService} from "../../core/auth/auth-jwt.service";
import {Router} from "@angular/router";
import {AuthenticationRequestBean} from "../../../gs-api/src/models/authentication-request-bean";
import {AuthenticateApiService} from "../../../gs-api/src/services/authenticate-api.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  errorMessage = '';
  isSaving = false;

  loginForm = this.fb.group({
    login: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
    password: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
  });

  constructor(
    private fb: FormBuilder,
    private authJwtService: AuthJwtService,
    private router: Router,
    private toastService: ToastrService,
    private authenticationService: AuthenticateApiService,
  ) {}



  loginWithUsernameAndPassword(): void {
    this.isSaving = true;
    this.authenticationService.authenticateByLoginAndPassword(this.saveValue()).subscribe(
      (response) => {
        this.isSaving = false;
        this.authJwtService.authenficationSuccess(response);
        this.router.navigate(['member/dashboard']).then(() => {});
        this.errorMessage = '';

    }, error => {
      this.errorMessage = error?.error?.message;
        this.isSaving = false;
        this.toastService.error(error?.error?.message);
    })
  }

  saveValue(): AuthenticationRequestBean {
    return {
      login: this.loginForm?.get('login')?.value ?? '',
      password: this.loginForm?.get('password')?.value ?? '',
    }
  }

}
